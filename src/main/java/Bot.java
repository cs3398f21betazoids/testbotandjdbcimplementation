import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;
import javax.security.auth.login.LoginException;

public class Bot {
    public static void main(String[] args) throws LoginException, InterruptedException {
        JDBC jdbc = new JDBC();
        jdbc.createTable();
        jdbc.InsertTransaction("INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) " +
                "VALUES (1, 'Paul', 32, 'California', 20000.00 );");
        jdbc.InsertTransaction("INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) " +
                "VALUES (2, 'Allen', 25, 'Texas', 15000.00 );");
        jdbc.InsertTransaction("INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) " +
                "VALUES (3, 'Teddy', 23, 'Norway', 20000.00 );");
        jdbc.InsertTransaction("INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) " +
                "VALUES (4, 'Mark', 25, 'Rich-Mond ', 65000.00 );");
        JDA jda = JDABuilder.createDefault("ODk0MDMyODk4Mjc2MDkzOTU0.YVkHBQ.2QuYBciMl42wcAVWA-_B8AaBGf4")
                .addEventListeners(new QuoteCmd(jdbc))
                .setActivity(Activity.listening("to your problems"))
                .setStatus(OnlineStatus.ONLINE)
                .build();
        jda.awaitReady();
    }
}

class QuoteCmd extends ListenerAdapter {
    JDBC jdbc;
    public QuoteCmd(JDBC jdbc) {
        this.jdbc = jdbc;
    }
    @Override
    public void onMessageReceived(@NotNull MessageReceivedEvent event) {
        super.onMessageReceived(event);
        // This statement checks if the author of the message
        // sent is a bot, and if the author of the message is
        // a bot, it doesnt respond.
        if(event.getAuthor().isBot()) {
            return;
        }

        // This if statement checks if the message is in DMs and
        // ignores it if it is, if you dont want that to happen,
        // delete or modify this line to your needs
        if(event.isFromType(ChannelType.PRIVATE)) {
            return;
        }

        // Gets the message that triggered the event
        Message msg = event.getMessage();

        if(msg.getContentRaw().equals("-hello neighbor"))
        // msg.getContentRaw() gets the message's content
        // with the markdown and hidden characters, even the
        // \ if it happened to be after a symbol, in which
        // Discord makes it disappear.
        {
            MessageChannel channel = event.getChannel();
            channel.sendMessage("Often when you think you're at the end of something, you're at the beginning of something else.").queue();
            // Important to call .queue()
        } else if (msg.getContentRaw().equals("-loveyou")) {
            if (msg.getAuthor().getName().equals("MDHS")) {
                MessageChannel channel = event.getChannel();
                channel.sendMessage("Stephen, I wish your bot was nicer.\n").queue();
            } else if (msg.getAuthor().getName().equals("Zett")) {
                MessageChannel channel = event.getChannel();
                channel.sendMessage("You have made a very nice bot, Zett.\n").queue();
            } else if (msg.getAuthor().getName().equals("Nike")) {
                MessageChannel channel = event.getChannel();
                channel.sendMessage("Nick, did you ever buy that bike you wanted as a kid?\n").queue();
            } else if (msg.getAuthor().getName().equals("Yrua")) {
                MessageChannel channel = event.getChannel();
                channel.sendMessage("OMG get back to your interviews!!!\n").queue();
            } else {
                MessageChannel channel = event.getChannel();
                channel.sendMessage("Oh god, who am I?\n").queue();
            }
            System.out.println(msg.getAuthor().getName());
            MessageChannel channel = event.getChannel();
            channel.sendMessage("And I love you, just the way you are.").queue();
        } else if (msg.getContentRaw().equals("-getsql")) {
            MessageChannel channel = event.getChannel();
            String results = jdbc.selectStatement("SELECT * FROM COMPANY");
            System.out.println(results);
           channel.sendMessage(results).queue();
        }
    }

}

