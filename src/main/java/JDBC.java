import java.sql.*;

public class JDBC {
    private Connection c = null;

    public JDBC() {}

    private  void openDataBase() {
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:test.db");
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
    }

    private  void closeDataBase() throws SQLException {
        c.close();
    }

    public void createTable() {
        Statement s;
        try {
            openDataBase();
            s = c.createStatement();
            String sql = "CREATE TABLE IF NOT EXISTS  COMPANY " +
                    "(ID INT PRIMARY KEY     NOT NULL," +
                    " NAME           TEXT    NOT NULL, " +
                    " AGE            INT     NOT NULL, " +
                    " ADDRESS        CHAR(50), " +
                    " SALARY         REAL)";
            s.executeUpdate(sql);
            s.close();
            c.close();
            System.out.println("Table created successfully");
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
        try {
            closeDataBase();

        } catch (SQLException se) {
            System.out.println("Unable to close connection to database.");
        }
    }

    public void InsertTransaction(String sql){
        Statement s;

        openDataBase();
        try {
            c.setAutoCommit(false);
            s = c.createStatement();
            s.executeUpdate(sql);
            s.close();
            c.commit();
            System.out.println("Record created successfully");
            closeDataBase();
        } catch (SQLException se) {
            System.out.println("Unable to set property for database");
        }
    }

    public String selectStatement(String query) {
        Statement s;
        StringBuilder sb = new StringBuilder();

        openDataBase();
        try {
            c.setAutoCommit(false);
            s = c.createStatement();
            ResultSet rs = s.executeQuery(query);
            while ( rs.next() ) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                int age = rs.getInt("age");
                String address = rs.getString("address");
                float salary = rs.getFloat("salary");
                sb.append("ID = ").append(id).append("\n")
                        .append("NAME = ").append(name).append("\n")
                        .append("AGE = ").append(age).append("\n")
                        .append("ADDRESS = ").append(address).append("\n")
                        .append("SALARY = ").append(salary).append("\n\n");

            }
            rs.close();
            s.close();
            closeDataBase();
            System.out.println("Selection was successful");
        } catch (SQLException se) {
            System.out.println("Unable to read from database");
        }
        return sb.toString();
    }
}